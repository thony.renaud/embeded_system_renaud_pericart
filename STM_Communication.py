import serial
# Ouvrir le port série
ser = serial.Serial('/dev/ttyACM0', 9600)  # Assurez-vous que le port série est correctement configuré
ser.flushInput()

try:
    while True:
        # Envoyer des données à la STM32

        ser.write(b'HELLO STM32\n')

        # Lire les données de la STM32
        response = ser.readline().decode('utf-8').rstrip()
        print("Réponse de la STM32:", response)
except KeyboardInterrupt:
    ser.close()  # Fermer le port série en cas d'interruption
