#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <console_io.h>
#include <sensehat.h>
//#include <wiringPi.h>  // Utilisez cette bibliothèque pour la communication série
#include <libusb-1.0/libusb.h>

using namespace std;
using namespace std::this_thread;  // sleep_for, sleep_until
using namespace std::chrono;       // nanoseconds, system_clock, seconds

int main() {
    unsigned int time = 0;
	double Temp;
	if (senseInit()) {
            cout << "-------------------------------" << endl << "Sense Hat initialization Ok." << endl;
            senseClear();
        for (time = 0; time < 60; time++) {
            sleep_for(milliseconds(500));

            Temp = senseGetTemperatureFromHumidity();
            cout << fixed << setprecision(2) << "Temp (from humid) = " << Temp
                 << "°C" << endl;
        }
        cout << endl << "Waiting for keypress." << endl;
        getch();
        senseShutdown();
        cout << "-------------------------------" << endl << "Sense Hat shut down." << endl;
    }

	return EXIT_SUCCESS;

}




/*
using namespace std;
using namespace std::this_thread;
using namespace std::chrono;

int main() {
    unsigned int time = 0;
    double Temp;

    if (senseInit()) {
        cout << "-------------------------------" << endl << "Sense Hat initialization Ok." << endl;
        senseClear();

        // Initialisez la communication série sur le port UART
        int uart_fd = serialOpen("/dev/ttyAMA0", 115200); // Remplacez "/dev/ttyAMA0" par le port UART correct

        for (time = 0; time < 60; time++) {
            sleep_for(milliseconds(500));

            Temp = senseGetTemperatureFromHumidity();
            cout << fixed << setprecision(2) << "Temp (from humid) = " << Temp << "°C" << endl;

            // Envoyez la température à la carte STM32 via l'UART
            serialPrintf(uart_fd, "%.2f\n", Temp);
        }

        cout << endl << "Waiting for keypress." << endl;
        getch();
        senseShutdown();
        cout << "-------------------------------" << endl << "Sense Hat shut down." << endl;

        // Fermez la communication série avant de quitter le programme
        serialClose(uart_fd);
    }

    return EXIT_SUCCESS;
}*/




/*
#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <sensehat.h>
#include <libusb-1.0/libusb.h>

using namespace std;
using namespace std::this_thread;
using namespace std::chrono;

int main() {
    unsigned int time = 0;
    double Temp;

    // Initialize USB communication
    libusb_context* ctx = nullptr;
    if (libusb_init(&ctx) != 0) {
        cerr << "Failed to initialize libusb" << endl;
        return EXIT_FAILURE;
    }

    libusb_device_handle* dev_handle = libusb_open_device_with_vid_pid(ctx, VENDOR_ID, PRODUCT_ID);
    if (!dev_handle) {
        cerr << "Failed to open USB device" << endl;
        libusb_exit(ctx);
        return EXIT_FAILURE;
    }

    // Your vendor ID and product ID should be replaced with the actual values of your STM card.
    const uint16_t VENDOR_ID = 0xXXXX;
    const uint16_t PRODUCT_ID = 0xXXXX;

    if (senseInit()) {
        cout << "-------------------------------" << endl << "Sense Hat initialization Ok." << endl;
        senseClear();
        for (time = 0; time < 60; time++) {
            sleep_for(milliseconds(500));

            Temp = senseGetTemperatureFromHumidity();
            cout << fixed << setprecision(2) << "Temp (from humid) = " << Temp << "°C" << endl;

            // Send temperature data over USB
            int bytes_sent;
            if (libusb_bulk_transfer(dev_handle, ENDPOINT_OUT, reinterpret_cast<unsigned char*>(&Temp), sizeof(Temp), &bytes_sent, 0) != 0) {
                cerr << "Failed to send data over USB" << endl;
            }
        }

        cout << endl << "Waiting for keypress." << endl;
        getch();
        senseShutdown();
        cout << "-------------------------------" << endl << "Sense Hat shut down." << endl;
    }

    // Close USB communication
    libusb_close(dev_handle);
    libusb_exit(ctx);

    return EXIT_SUCCESS;
}
*/



/*
 * libusb example program to list devices on the bus
 * Copyright (C) 2007 Daniel Drake <dsd@gentoo.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
/*#include <stdio.h>
#include <sys/types.h>
#include <libusb/libusb.h>
static void print_devs(libusb_device **devs)
{
    libusb_device *dev;
    int i = 0;
    while ((dev = devs[i++]) != NULL) {
        struct libusb_device_descriptor desc;
        int r = libusb_get_device_descriptor(dev, &desc);
        if (r < 0) {
            fprintf(stderr, "failed to get device descriptor");
            return;
        }
        printf("%04x:%04x (bus %d, device %d)\n",
            desc.idVendor, desc.idProduct,
            libusb_get_bus_number(dev), libusb_get_device_address(dev));
    }
}
int main(void)
{
    libusb_device **devs;
    int r;
    ssize_t cnt;
    r = libusb_init(NULL);
    if (r < 0)
        return r;
    cnt = libusb_get_device_list(NULL, &devs);
    if (cnt < 0)
        return (int) cnt;
    print_devs(devs);
    libusb_free_device_list(devs, 1);
    libusb_exit(NULL);
    return 0;
}*/
